package com.example.nombreperro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    TextView tv;
    String somb;
    String imc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        tv=findViewById(R.id.textView);

        somb=getIntent().getExtras().getString("Valor");
        imc=getIntent().getExtras().getString("Peso");

        tv.setText("Tu humano tiene un imc de "+imc+" y su sombrero tiene un area de "+somb+" cm.");
    }
}
