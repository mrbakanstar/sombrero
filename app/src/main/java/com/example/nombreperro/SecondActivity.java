package com.example.nombreperro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    EditText Altura;
    EditText Peso;
    Button btn;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Altura = findViewById (R.id.Altura);
        Peso = findViewById (R.id.Base);
        btn = findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(SecondActivity.this,ThirdActivity.class);
                double altura = Integer.parseInt(Peso.getText().toString());
                int peso = Integer.parseInt(Altura.getText().toString());

                result = altura / peso;

                i.putExtra("Peso",result);
                startActivity(i);
                finish();
            }
        }
        );










    }
}
