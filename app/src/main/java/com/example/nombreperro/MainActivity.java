package com.example.nombreperro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText Altura;
    EditText Base;
    Button btn;
    int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Altura = findViewById (R.id.Altura);
        Base = findViewById (R.id.Base);
        btn = findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(MainActivity.this,SecondActivity.class);

                int base = Integer.parseInt(Base.getText().toString());
                int altura = Integer.parseInt(Altura.getText().toString());

                result = (base * altura) / 2;

                i.putExtra("Valor",result);
                startActivity(i);
                finish();
            }
        }
        );










    }
}